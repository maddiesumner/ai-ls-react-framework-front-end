/*!

=========================================================
* Light Bootstrap Dashboard React - v1.3.0
=========================================================

* Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { Component } from "react";
import { NavItem, Nav, NavDropdown, MenuItem } from "react-bootstrap";

class AdminNavbarLinks extends Component {
  render() {
    return (
      <div>
        <Nav pullRight>
          <NavDropdown
            eventKey={1}
            title="Links"
            id="basic-nav-dropdown-right"
          >
            <MenuItem eventKey={1.1} href="#">Link 1</MenuItem>
            <MenuItem eventKey={1.2} href="#">Link 2</MenuItem>
            <MenuItem eventKey={1.3} href="#">Link 3</MenuItem>
            <MenuItem divider />
            <MenuItem eventKey={1.5} href="https://labs.telstra.com/" target="_blank">Telstra Labs</MenuItem>
          </NavDropdown>
        </Nav>
      </div>
    );
  }
}

export default AdminNavbarLinks;
